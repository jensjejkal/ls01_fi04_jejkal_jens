﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       //double Variablen
       double zuZahlenderBetrag;
       double eingezahlterGesamtbetrag;
       do {
    	   System.out.println("------------------");
    	   System.out.println("Neue Fahrkarte");
    	   System.out.println("------------------\n");
    	   zuZahlenderBetrag = fahrkartenbestellungErfassen();
       
    	   warte(1000);
       
    	   // Geldeinwurf
    	   // -----------
    	   eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       
    	   // Fahrscheinausgabe
    	   // -----------------
    	   fahrkartenAusgeben();
       
       

    	   // Rückgeldberechnung und -Ausgabe
    	   // -------------------------------
    	   rueckgeldAusgeben(zuZahlenderBetrag, eingezahlterGesamtbetrag);

    	   System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          	  "vor Fahrtantritt entwerten zu lassen!\n"+
                              "Wir wünschen Ihnen eine gute Fahrt.");
       } while (true);
    }
    public static double fahrkartenbestellungErfassen() {
    	double preisFuerEinTicket = 0;
    	int ArrayLength = 10;
    	
    	Scanner tastatur = new Scanner(System.in);
    	String[] fahrkartenName = new String[ArrayLength];
    	Double[] fahrkartenPreis = new Double[ArrayLength];
    	
    	fahrkartenName[0] = "Einzelfahrschein Berlin AB";
    	fahrkartenName[1] = "Einzelfahrschein Berlin BC"; 
    	fahrkartenName[2] = "Einzelfahrschein Berlin ABC"; 
    	fahrkartenName[3] = "Kurzstrecke";
    	fahrkartenName[4] = "Tageskarte Berlin AB";
    	fahrkartenName[5] = "Tageskarte Berlin BC";
    	fahrkartenName[6] = "Tageskarte Berlin ABC";
    	fahrkartenName[7] = "Kleingruppen-Tageskarte Berlin AB";
    	fahrkartenName[8] = "Kleingruppen-Tageskarte Berlin BC";
    	fahrkartenName[9] = "Kleingruppen-Tageskarte Berlin ABC";
    	
    	fahrkartenPreis[0] = 2.90;
    	fahrkartenPreis[1] = 3.30;
    	fahrkartenPreis[2] = 3.60;
    	fahrkartenPreis[3] = 1.90;
    	fahrkartenPreis[4] = 8.60;
    	fahrkartenPreis[5] = 9.00;
    	fahrkartenPreis[6] = 9.60;
    	fahrkartenPreis[7] = 23.50;
    	fahrkartenPreis[8] = 24.30;
    	fahrkartenPreis[9] = 24.90;
    	
    	for(int i = 0; i < fahrkartenName.length; i++) {
    		System.out.println(i+1 + "\t" + fahrkartenName[i] + "\t" + fahrkartenPreis[i] + "€");
    	}
    	
    	System.out.printf("\nGeben Sie die Auswahl ein: ");
        int benutzerwahl = tastatur.nextInt() - 1;
        boolean benutzerEingabe = false;
        while(!benutzerEingabe) {
         if (benutzerwahl < 0) {
        	System.out.printf("Zu kleine Auswahl. Geben Sie die Auswahl auf der Liste ein: ");
        	benutzerwahl = tastatur.nextInt() - 1;
        }
         else if (benutzerwahl > fahrkartenName.length - 1) {
        	System.out.printf("Zu große Auswahl. Geben Sie die Auswahl auf der Liste ein: ");
        	benutzerwahl = tastatur.nextInt() - 1;
        }
         else {
         benutzerEingabe = true;
         System.out.printf("Die Auswahl ist %s\n", fahrkartenName[benutzerwahl]);
         preisFuerEinTicket = fahrkartenPreis[benutzerwahl];
        }}
    	
    	int TicketAnzahl;
    	double zuZahlenderBetrag = 0;
        System.out.printf("Anzahl der Tickets: ");
        TicketAnzahl = tastatur.nextInt();
        boolean TicketNotValid = true;
        
        
        while (TicketNotValid == true) {
         if(TicketAnzahl > 10) {
        	TicketNotValid = true;
        	System.out.println("Zu hoher Ticketwert. Bitte eine Ticketanzahl von 1 bis 10 angeben");
        	 try {
           	   Thread.sleep(700);
              } catch (InterruptedException e) {
           	   e.printStackTrace();
    		 }
        	System.out.printf("Anzahl der Tickets: ");
            TicketAnzahl = tastatur.nextInt();
           
         }
         else if(TicketAnzahl < 1) {
        	TicketNotValid = true;
        	System.out.println("Zu niedriger Ticketwert.  Bitte eine Ticketanzahl von 1 bis 10 angeben");
        	 try {
           	   Thread.sleep(700);
              } catch (InterruptedException e) {
           	   e.printStackTrace();
    		  }
        	System.out.printf("Anzahl der Tickets: ");
            TicketAnzahl = tastatur.nextInt();
           
          }
          else {
        	TicketNotValid = false;
        	zuZahlenderBetrag = preisFuerEinTicket * TicketAnzahl;  
          }
         
        }
       return zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	double eingezahlterGesamtbetrag;
    	double eingeworfeneMünze;
    	Scanner tastatur = new Scanner(System.in);
    	
    	eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	  double Rest = zuZahlenderBetrag - eingezahlterGesamtbetrag;
     	  System.out.printf("Noch zu zahlen: " + "%.2f" + " EURO\n", Rest);
     	  System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	  eingeworfeneMünze = tastatur.nextDouble();
          eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
       return eingezahlterGesamtbetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) //int counter
        {
           System.out.print("=");
           try {
        	   Thread.sleep(250);
           } catch (InterruptedException e) {
 		// TODO Auto-generated catch block
        	   e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    	
    }
    
    public static void rueckgeldAusgeben(double zuZahlenderBetrag, double eingezahlterGesamtbetrag) {
    	double rückgabebetrag;
    	
    	rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag; //double
        if(rückgabebetrag > 0.0)
        {
     	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
              muenzenAusgeben(2, "EURO");
 	          rückgabebetrag -= 2.0;
             }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
              muenzenAusgeben(1, "EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
              muenzenAusgeben(50, "CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
              muenzenAusgeben(20, "CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
              muenzenAusgeben(10, "CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag > 0.04)// 5 CENT-Münzen
            {
             muenzenAusgeben(5, "CENT");
             rückgabebetrag -= 0.05;
  	          
            }
        }
    }
   public static void warte(int millisekunden) {
    	
        try {
     	   Thread.sleep(millisekunden);
        } catch (InterruptedException e) {
		// TODO Auto-generated catch block
     	   e.printStackTrace();
        }
   }
        
   public static void muenzenAusgeben(int betrag, String einheit) {
       System.out.printf("%d %s\n", betrag, einheit);
        	
        
    }    
        
}