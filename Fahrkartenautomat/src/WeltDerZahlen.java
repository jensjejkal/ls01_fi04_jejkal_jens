/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnensystem                    
    byte anzahlPlaneten = 8;
    
    // Anzahl der Sterne in unserer Milchstraße
        long anzahlSterne = 100_000_000_000L;
    
    // Wie viele Einwohner hat Berlin?
        int bewohnerBerlin = 3769000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
       short alterTage = 7862;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
       int gewichtKilogramm = 200000;  
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
       int flaecheGroessteLand = 17098242;
    
    // Wie groß ist das kleinste Land der Erde?
    
       double flaecheKleinsteLand = 0.44;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    
    System.out.println("Bewohner Berlin: " + bewohnerBerlin);
    
    System.out.println("Alter in Tage: " + alterTage);
    
    System.out.println("schwerstes Tier: " + gewichtKilogramm);
    
    System.out.println("Gr��stes Land Fl�che: " + flaecheGroessteLand);
    
    System.out.println("kleinstes Lnad Fl�che: " + flaecheKleinsteLand);    
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

