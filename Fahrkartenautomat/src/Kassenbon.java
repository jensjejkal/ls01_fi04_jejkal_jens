import java.util.Scanner;

public class Kassenbon {

	public static void main(String[] args) {

		// Benutzereingaben lesen
		
		String artikel = liesString("Was m�chten Sie bestellen?");
		int anzahl = liesInt("Geben Sie die Anzahl ein: ");
		double preis = liesDouble("Geben Sie den Nettopreis ein: ");
		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein: ");

		// Verarbeiten
		
		double nettogesamtpreis = nettopreisrechnung(anzahl, preis);
		double bruttogesamtpreis = bruttopreisrechnung(nettogesamtpreis, mwst);
		
		// Ausgeben

		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");


	}
	public static String liesString(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		String str = myScanner.next();
		return str;
	}
	public static int liesInt(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		int number = myScanner.nextInt();
		return number;
	}
	public static double liesDouble(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		double DZahl = myScanner.nextDouble();
		return DZahl;
	}
	public static double nettopreisrechnung(double zahl, double preis) {
			return zahl * preis;
	}
	public static double bruttopreisrechnung(double nettopreisrechnung, double mwst) {
		return nettopreisrechnung * (1 + mwst / 100);
	}
}
