package uh;

public class Temperatur {

	public static void main(String[] args) {
		int F1 = -20;
		int F2 = -10;
		int F3 = 0;
		int F4 = 20;
		int F5 = 30;
		double C1 = -28.8889;
		double C2 = -23.3333;
		double C3 = -17.7778;
		double C4 = -6.6667;
		double C5 = -1.1111;
		String s1 = "Fahrenheit";
		String s2 = "Celsius";
		String U = "---------------------------";
		String Null = "";
		String Plus = "+";
		
		System.out.printf("%-12s|%9s\n", s1, s2);
		System.out.printf("%.23s\n", U);
		System.out.printf("%-12d|%3s%.2f\n", F1, Null, C1);
		System.out.printf("%-12d|%3s%.2f\n", F2, Null, C2);
		System.out.printf("%s%-11d|%3s%.2f\n", Plus, F3, Null, C3);
		System.out.printf("%s%-11d|%4s%.2f\n", Plus, F4, Null, C4);
		System.out.printf("%s%-11d|%4s%.2f\n", Plus, F5, Null, C5);
		

	}

}
